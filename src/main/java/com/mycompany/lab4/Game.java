/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author zacop
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('X');
        this.player2 = new Player('O');
    }

    public void newGame(){
        this.table = new Table(player1,player2);
    }
    
    public void play() {
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
                showWin();
                saveWin();
                showInfo();
                newGame();
                table.switchPlayer();
                
                
            }
            if(table.checkDraw()){
                showTable();
                showDraw();
                saveDraw();
                showInfo();
                newGame();
                table.switchPlayer();
            }
            table.switchPlayer();
        }
        
    }

    public void showWelcome() {
        System.out.println("Welcome to XO Game");
    }

    public void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int r = 0; r < 3; r++) {
                System.out.print(t[i][r] + " ");
            }
            System.out.println();
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol());
    }
    
    public void showWin(){
        System.out.println(table.getCurrentPlayer().getSymbol()+" Win!");
    }
    
    public void showDraw(){
        System.out.println("Draw!");
    }
    
    private void showInfo(){
        System.out.println(player1);
        System.out.println(player2);
    }
    
    public void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input Row and Col: ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        if(table.setRowCol(row,col) == false){
            System.out.println("Please select a space!");
            row = sc.nextInt();
            col = sc.nextInt();
            table.setRowCol(row,col);
        }
    }
    
    public void saveWin(){
        if(player1 == table.getCurrentPlayer()){
            player1.countWin();
            player2.countLose();
        }else{
            player2.countWin();
            player1.countLose();
        }
    }

    public void saveDraw(){
        player1.countDraw();
        player2.countDraw();
    }
}
