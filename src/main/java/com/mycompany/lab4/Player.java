/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author zacop
 */
public class Player {
    private char symbol;
    private int win,lose,draw;
    
    public Player(char symbol){
        this.symbol = symbol;
        this.win = win;
        this.lose = lose;
        this.draw = draw;
    } 
    
    public char getSymbol(){
        return symbol;
    }
    
    public void countWin(){
        win++;
    }
    
    public void countLose(){
        lose++;
    }
    
    public void countDraw(){
        draw++;
    }
    
    public int getWin(){
        return win;
    }
    
    public int getLose(){
        return lose;
    }
    
    public int getDraw(){
        return draw;
    }

    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
}
